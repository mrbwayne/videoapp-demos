//
//  TrimVideoClass.h
//  trimvideo
//
//  Created by Anuj on 6/2/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>

@interface TrimVideoClass : NSObject
+(void)trimVideo:(NSURL*)videoToTrimURL StartTimeInMinute:(float)startMinute EndTimeInMinute:(float)endMinute;
+(void)mergeVideos:(NSString*)videoBundleURL1 Video2Path:(NSString*)videoBundleURL2  StartMinute:(float)startMinute EndTimeInMinute:(float)endMinute;
+(void)makeFilm:(NSMutableArray*)videoClipPaths;
@end
