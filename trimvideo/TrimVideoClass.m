//
//  TrimVideoClass.m
//  trimvideo
//
//  Created by Anuj on 6/2/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import "TrimVideoClass.h"

@implementation TrimVideoClass


+(void)trimVideo:(NSURL*)videoToTrimURL StartTimeInMinute:(float)startMinute EndTimeInMinute:(float)endMinute
{
    @autoreleasepool {
        
        
        float start_time=startMinute * 60;
        float end_time=endMinute * 60;
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoToTrimURL options:nil];
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        documentsPath=[documentsPath stringByAppendingString:@"/TrimVideos"];
        
        NSString *outputURL = documentsPath;
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
        outputURL = [outputURL stringByAppendingPathComponent:@"trim.mov"];
        // Remove Existing File
        [manager removeItemAtPath:outputURL error:nil];
        
        NSURL* outputUrl = [NSURL fileURLWithPath:outputURL];
        
        
        exportSession.outputURL = outputUrl;
        exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        //CMTime start = CMTimeMakeWithSeconds(120.0, 600);
        // CMTime duration = CMTimeMakeWithSeconds(180.0, 600);
        // CMTime start = CMTimeMakeWithSeconds(120.0, 1);
        // CMTime duration = CMTimeMakeWithSeconds((240.0 - 120.0), 1);
        AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:videoToTrimURL options:nil];
        
        CMTime start = CMTimeMakeWithSeconds(start_time, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(end_time-start_time, anAsset.duration.timescale);
        
        CMTimeRange range = CMTimeRangeMake(start, duration);
        exportSession.timeRange = range;
        [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
         {
             switch (exportSession.status) {
                 case AVAssetExportSessionStatusCompleted:
                     [self writeVideoToPhotoLibrary:[NSURL fileURLWithPath:outputURL]];
                     NSLog(@"Export Complete %ld %@", (long)exportSession.status, exportSession.error);
                     break;
                 case AVAssetExportSessionStatusFailed:
                     NSLog(@"Failed:%@",exportSession.error);
                     break;
                 case AVAssetExportSessionStatusCancelled:
                     NSLog(@"Canceled:%@",exportSession.error);
                     break;
                 default:
                     break;
             }
             
             //[exportSession release];
         }];
    }
}
+(void)writeVideoToPhotoLibrary:(NSURL *)nsurlToSave{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    NSURL *recordedVideoURL= nsurlToSave;
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:recordedVideoURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:recordedVideoURL completionBlock:^(NSURL *assetURL, NSError *error){
        }];
    }
}

+(void)mergeVideos:(NSString*)videoBundleURL1 Video2Path:(NSString*)videoBundleURL2  StartMinute:(float)startMinute EndTimeInMinute:(float)endMinute
{
    @autoreleasepool {
        
    
     NSMutableArray  *videoClipPaths=[[NSMutableArray alloc]init];
    
  
    [videoClipPaths addObject:[NSURL fileURLWithPath:videoBundleURL1]];
    [videoClipPaths addObject:[NSURL fileURLWithPath:videoBundleURL2]];
    [videoClipPaths addObject:[NSURL fileURLWithPath:videoBundleURL1]];
    
    float start_time=startMinute * 60;
    float end_time=endMinute * 60;
    AVMutableComposition *mixComposition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *compositionTrack2 = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    for (int i=0; i<[videoClipPaths count]; i++)
    {
        AVURLAsset *assetClip = [AVURLAsset URLAssetWithURL:[videoClipPaths objectAtIndex:i] options:nil];
        AVAssetTrack *clipVideoTrackB = [[assetClip tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:[videoClipPaths objectAtIndex:i] options:nil];
        CMTime start;
        CMTime duration;
        CMTimeRange video_timeRange;
        if (i==0){
            start = CMTimeMakeWithSeconds(0.0, anAsset.duration.timescale);
            duration = CMTimeMakeWithSeconds(start_time, anAsset.duration.timescale);
            
        }else if (i==1){
            start = CMTimeMakeWithSeconds(start_time, anAsset.duration.timescale);
            duration = CMTimeMakeWithSeconds(end_time-start_time, anAsset.duration.timescale);
        }
        else if (i==2){
            start = CMTimeMakeWithSeconds(end_time, anAsset.duration.timescale);
            duration = CMTimeMakeWithSeconds(anAsset.duration.timescale, anAsset.duration.timescale);
        }
        video_timeRange = CMTimeRangeMake(start,duration);
        [compositionTrack insertTimeRange:video_timeRange ofTrack:clipVideoTrackB atTime:start error:nil];
        
        //merge audio of video files
        AVAssetTrack *clipVideoTrackB1 = [[assetClip tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        /*CMTime start1;
        CMTime duration1;
        CMTimeRange video_timeRange1;
        if (i==0){
            start1 = CMTimeMakeWithSeconds(0.0, anAsset.duration.timescale);
            duration1 = CMTimeMakeWithSeconds(start_time, anAsset.duration.timescale);
            
        }else if (i==1){
            start1 = CMTimeMakeWithSeconds(start_time, anAsset.duration.timescale);
            duration1 = CMTimeMakeWithSeconds(end_time-start_time, anAsset.duration.timescale);
        }
        else if (i==2){
            start1 = CMTimeMakeWithSeconds(end_time, anAsset.duration.timescale);
            duration1 = CMTimeMakeWithSeconds(anAsset.duration.timescale, anAsset.duration.timescale);
        }
        video_timeRange1 = CMTimeRangeMake(start,duration);*/
        [compositionTrack2 insertTimeRange:video_timeRange ofTrack:clipVideoTrackB1 atTime:start error:nil];
        
    }
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    NSParameterAssert(exporter != nil);
  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    documentsPath=[documentsPath stringByAppendingString:@"/MergeVideos"];
    
    NSString *outputURL = documentsPath;
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    outputURL = [outputURL stringByAppendingPathComponent:@"merge.mov"];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    
    NSURL* outputUrl = [NSURL fileURLWithPath:outputURL];
        
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.outputURL = outputUrl;
    [exporter exportAsynchronouslyWithCompletionHandler:^(void){
        switch (exporter.status) {
            case AVAssetExportSessionStatusFailed:
                NSLog(@"exporting failed");
                break;
            case AVAssetExportSessionStatusCompleted:
                NSLog(@"exporting completed");
                //UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, nil, NULL);
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"export cancelled");
                break;
        }
    }];
 }
}

+(void)makeFilm:(NSMutableArray*)videoClipPaths
{
    @autoreleasepool {
        
        
       
        AVMutableComposition *mixComposition = [AVMutableComposition composition];
        AVMutableCompositionTrack *compositionTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        AVMutableCompositionTrack *compositionTrack2 = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        
         float start_time_vid=0.0;
        
      /*
        for (int i=0; i<[videoClipPaths count]; i++)
        {
            NSString *strNamePath=[[videoClipPaths objectAtIndex:i] absoluteString];
            NSLog(@"video path name>>%@",strNamePath);
            AVURLAsset *assetClip = [AVURLAsset URLAssetWithURL:[videoClipPaths objectAtIndex:i] options:nil];
            AVAssetTrack *clipVideoTrackB = [[assetClip tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
            AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:[videoClipPaths objectAtIndex:i] options:nil];
            CMTime start;
            CMTime duration;
            CMTimeRange video_timeRange;
            if (i==0){
                start = CMTimeMakeWithSeconds(start_time_vid, 600.0);
                duration = CMTimeMakeWithSeconds(anAsset.duration.timescale, 600.0);
                
            }else {
                start = CMTimeMakeWithSeconds(start_time_vid, 600.0);
                duration = CMTimeMakeWithSeconds(anAsset.duration.timescale, 600.0);
            }
            
            video_timeRange = CMTimeRangeMake(start,duration);
            [compositionTrack insertTimeRange:video_timeRange ofTrack:clipVideoTrackB atTime:start error:nil];
            
             CMTime start1;
             CMTime duration1;
             CMTimeRange video_timeRange1;
             if (i==0){
             start1 = CMTimeMakeWithSeconds(start_time_vid, 600.0);
             duration1 = CMTimeMakeWithSeconds(anAsset.duration.timescale, 600.0);
             }else {
             start1 = CMTimeMakeWithSeconds(0, 600.0);
             duration1 = CMTimeMakeWithSeconds(anAsset.duration.timescale, 600.0);
             }
             
             
             //merge audio of video files
             if ([strNamePath rangeOfString:@"assets"].location != NSNotFound) {
             NSLog(@"AUDIO??????????????????????");
             AVAssetTrack *clipVideoTrackB1 = [[assetClip tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
             video_timeRange1 = CMTimeRangeMake(start,duration);
             [compositionTrack2 insertTimeRange:video_timeRange1 ofTrack:clipVideoTrackB1 atTime:start1 error:nil];
             }
            float seconds = CMTimeGetSeconds(anAsset.duration);
            NSLog(@"duration: %f", seconds);
            start_time_vid = start_time_vid + seconds;
        }
        */
        NSMutableArray * timeRanges = [NSMutableArray arrayWithCapacity:videoClipPaths.count];
        NSMutableArray * tracks = [NSMutableArray arrayWithCapacity:videoClipPaths.count];
        
        NSMutableArray * timeRangesAud = [[NSMutableArray alloc]init];
        NSMutableArray * tracksAud = [[NSMutableArray alloc]init];
        for (int i=0; i<[videoClipPaths count]; i++) {
            
            NSString *strNamePath=[(NSURL*)[videoClipPaths objectAtIndex:i] absoluteString];
            AVURLAsset *assetClip = [AVURLAsset URLAssetWithURL:[videoClipPaths objectAtIndex:i] options:nil];
            AVAssetTrack *clipVideoTrackB = [[assetClip tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
            [timeRanges addObject:[NSValue valueWithCMTimeRange:CMTimeRangeMake(kCMTimeZero, assetClip.duration)]];
            [tracks addObject:clipVideoTrackB];
           
            if ([strNamePath rangeOfString:@"assets"].location != NSNotFound) {
                 //merge audio of video files -->if any
                //CMTime start = CMTimeMakeWithSeconds(start_time_vid, 600.0);
                 AVAssetTrack *clipVideoTrackB1 = [[assetClip tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
                [timeRangesAud addObject:[NSValue valueWithCMTimeRange:CMTimeRangeMake(kCMTimeZero, assetClip.duration)]];
                [tracksAud addObject:clipVideoTrackB1];
            }
            float seconds = CMTimeGetSeconds(assetClip.duration);
           
            start_time_vid = start_time_vid + seconds;
             NSLog(@"start_time_vid: %.2f", start_time_vid);
            strNamePath=nil;assetClip=nil,clipVideoTrackB=nil;
        }
        [compositionTrack insertTimeRanges:timeRanges ofTracks:tracks atTime:kCMTimeZero error:Nil];
        [compositionTrack2 insertTimeRanges:timeRangesAud ofTracks:tracksAud atTime:kCMTimeZero error:Nil];

        tracks=nil,tracksAud=nil,timeRanges=nil,timeRangesAud=nil;
        
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
        
        NSParameterAssert(exporter != nil);
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        documentsPath=[documentsPath stringByAppendingString:@"/FilmStrip"];
        
        NSString *outputURL = documentsPath;
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
        outputURL = [outputURL stringByAppendingPathComponent:@"film.mov"];
        // Remove Existing File
        [manager removeItemAtPath:outputURL error:nil];
        
        NSURL* outputUrl = [NSURL fileURLWithPath:outputURL];
        
        exporter.outputFileType = AVFileTypeQuickTimeMovie;
        exporter.outputURL = outputUrl;
        [exporter exportAsynchronouslyWithCompletionHandler:^(void){
            switch (exporter.status) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"exporting failed");
                    break;
                case AVAssetExportSessionStatusCompleted:
                    NSLog(@"exporting completed");
                    //UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, nil, NULL);
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"export cancelled");
                    break;
            }
        }];
    }
}
@end
