//
//  scrolldrag.m
//  trimvideo
//
//  Created by Anuj on 6/9/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import "scrolldrag.h"


//#define kNumberOfCellsInRowForFilStripTable 14
#define kNumberOfCellsInRowForAssetTable 3
@implementation scrolldrag

-(void)viewDidLoad
{
   

    allVideos = [[NSMutableArray alloc] init];
    allFilmStrips=[[NSMutableArray alloc]init];
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
   __block int tag=0;
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop)
     {
         if (group)
         {
             [group setAssetsFilter:[ALAssetsFilter allAssets]];
             [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop)
              {
                  if (asset)
                  {
                      dic = [[NSMutableDictionary alloc] init];
                      ALAssetRepresentation *defaultRepresentation = [asset defaultRepresentation];
                      NSString *uti = [defaultRepresentation UTI];
                      NSURL  *videoURL = [[asset valueForProperty:ALAssetPropertyURLs] valueForKey:uti];
                      NSString *title = [NSString stringWithFormat:@"video %d", arc4random()%100];
                      UIImage *image;
                      if([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto])
                      {
                          image=[UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
                          [dic setValue:[NSString stringWithFormat:@"%d",1] forKey:@"isImage"];
                      }
                      else{
                          image = [self imageFromVideoURL:videoURL];
                          [dic setValue:[NSString stringWithFormat:@"%d",0] forKey:@"isImage"];
                          
                      }
                      [dic setValue:image forKey:@"image"];
                      [dic setValue:title forKey:@"name"];
                      [dic setValue:videoURL forKey:@"url"];
                      [dic setValue:[NSString stringWithFormat:@"%d",tag] forKey:@"tag"];
                      [allVideos addObject:dic];
                      //[allFilmStrips addObject:dic];
                      tag++;
                  }
                  else{
                      [tableview_gallary reloadData];
                      //[tableview_filmstrip reloadData];
                  }
              }];
         }
     }
                              failureBlock:^(NSError *error)
    {
        NSLog(@"error enumerating AssetLibrary groups %@\n", error);
    }];
    
    if (!tempImageView) {
        tempImageView=[[MyImageView alloc]init];
        [self.view bringSubviewToFront:tempImageView];
    }
}

- (UIImage *)imageFromVideoURL:(NSURL*)videoURL
{
    // result
    UIImage *image = nil;
    
    // AVAssetImageGenerator
    AVAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    
    // calc midpoint time of video
    Float64 durationSeconds = CMTimeGetSeconds([asset duration]);
    CMTime midpoint = CMTimeMakeWithSeconds(durationSeconds/2.0, 600);
    
    // get the image from
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef halfWayImage = [imageGenerator copyCGImageAtTime:midpoint actualTime:&actualTime error:&error];
    
    if (halfWayImage != NULL)
    {
        // CGImage to UIImage
        image = [[UIImage alloc] initWithCGImage:halfWayImage];
        [dic setValue:image forKey:@"name"];
        CGImageRelease(halfWayImage);
    }
    return image;
}

#pragma mark - Table view delegate and datasource implementations
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (tableView==tableview_filmstrip) {
         return 90;
    }
    else{
         return 100;
    }
   
}

-(NSInteger) tableView:(UITableView*) tableView numberOfRowsInSection:(NSInteger) section{
  
    int rowcount;
    if (tableView==tableview_filmstrip) {
        NSLog(@"allFilmStrips>>%@",allFilmStrips.description);
        kNumberOfCellsInRowForFilStripTable=allFilmStrips.count;
        rowcount=1;
    }
    else{
        if (allVideos.count==0)
            return allVideos.count;
        //NSLog(@"allvideos>>%@",allVideos.description);
        if(allVideos.count%kNumberOfCellsInRowForAssetTable==0)
        {
            rowcount = allVideos.count/kNumberOfCellsInRowForAssetTable;
        }
        else
        {
            rowcount = (allVideos.count/kNumberOfCellsInRowForAssetTable)+1;
        }
    }
    return rowcount;

}


-(UITableViewCell*) tableView:(UITableView*) tableView cellForRowAtIndexPath:(NSIndexPath*) indexPath
{
    
    @autoreleasepool {
        
    if (tableView==tableview_filmstrip) {
        if (allFilmStrips.count==0) {
            NSString *CellIdentifier = [NSString stringWithFormat:@"EmptyCell%ld",(long)indexPath.row];
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.text=@"Drop here....";
            return cell;
        }
        
        //NSString *CellIdentifier = [NSString stringWithFormat:@"FilmCell%ld%@",(long)indexPath.row,[[allFilmStrips objectAtIndex:indexPath.row] valueForKey:@"name"]];
        NSString *CellIdentifier = [NSString stringWithFormat:@"FilmCell"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell=nil;
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            if (allFilmStrips.count==0) {
                cell.textLabel.text=@"Drop here....";
                return cell;
            }
            
                int i;
                k=0;
                MyImageView *DisplayImageView;
                UIScrollView *scrollview = [[UIScrollView alloc]init];
                scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(5.0, 0.0, 300, 90)];
                for(i =0;i<kNumberOfCellsInRowForFilStripTable; i++)
               {
                if(indexPath.row==0){
                    k=indexPath.row+i;
                }
                else{
                    k=(indexPath.row*kNumberOfCellsInRowForFilStripTable)+i;
                }
                
                if(k==[allFilmStrips count])
                {
                    break;
                }
                else{
                    //DisplayImageView = [[UIImageView alloc]initWithFrame:CGRectMake(9+150*i,2,148,123)];
                     DisplayImageView = [[MyImageView alloc]initWithFrame:CGRectMake(9+85*i,2,75,75)];
                     DisplayImageView.userInteractionEnabled=TRUE;
                     DisplayImageView.dict=(NSMutableDictionary*)[allFilmStrips objectAtIndex:k];
                     DisplayImageView.myFrame=DisplayImageView.frame;
                   // if ([[[allFilmStrips objectAtIndex:k] valueForKey:@"isImage"] boolValue]){
                        DisplayImageView.image = (UIImage*)[[allFilmStrips objectAtIndex:k] valueForKey:@"image"];
                        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                              initWithTarget:self action:@selector(switchfilmstrip:)];
                        lpgr.minimumPressDuration = 1.0; //seconds
                        [DisplayImageView addGestureRecognizer:lpgr];
                        [scrollview addSubview:DisplayImageView];
                        scrollview.tag = 1;
                        scrollview.scrollEnabled=TRUE;
                        scrollview.autoresizingMask = TRUE;
                       
                    /*}else{
                        NSURL * imageUrl = (NSURL*)[[allFilmStrips objectAtIndex:k] valueForKey:@"url"];
                        
                        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
                        {
                            ALAssetRepresentation *rep = [myasset defaultRepresentation];
                            CGImageRef iref = [rep fullResolutionImage];
                            if (iref) {
                                UIImage *largeimage = [UIImage imageWithCGImage:iref];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *scaledimg_pic=[[largeimage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(100, 100) interpolationQuality:0] fixOrientation];
                                    DisplayImageView.image = scaledimg_pic;
                                    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                                          initWithTarget:self action:@selector(switchfilmstrip:)];
                                    lpgr.minimumPressDuration = 1.0; //seconds
                                    [DisplayImageView addGestureRecognizer:lpgr];
                                    [scrollview addSubview:DisplayImageView];
                                    scrollview.tag = 1;
                                    scrollview.scrollEnabled=TRUE;
                                    scrollview.autoresizingMask = TRUE;
                                    
                                });
                                
                            } else {
                                NSLog(@"Error in resultblock in PhotoAlbumViewController!");
                            }
                        };
                        
                        // failure block
                        ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
                        {
                            NSLog(@"Can't get image - %@",[myerror localizedDescription]);
                        };
                        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
                        [assetslibrary assetForURL:imageUrl
                                       resultBlock:resultblock
                                      failureBlock:failureblock];
                        
                    }*/
                    
                    scrollview.contentSize = CGSizeMake(DisplayImageView.frame.size.width+DisplayImageView.frame.origin.x,75);
                    [cell.contentView addSubview:scrollview];
                }
            }
            
        }
        return cell;

    }
    else{
     NSString *CellIdentifier = [NSString stringWithFormat:@"DisplayCell%ld",(long)indexPath.row];
    
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       
        int i;
        k=0;
        MyImageView *DisplayImageView;
        for(i =0;i<kNumberOfCellsInRowForAssetTable; i++)
        {
            if(indexPath.row==0){
                k=indexPath.row+i;
            }
            else{
                k=(indexPath.row*kNumberOfCellsInRowForAssetTable)+i;
            }
            
            if(k==[allVideos count])
            {
                break;
            }
            else{
               
                DisplayImageView = [[MyImageView alloc]initWithFrame:CGRectMake(25+90*i,10,80,80)];
                DisplayImageView.userInteractionEnabled=TRUE;
                DisplayImageView.dict=(NSMutableDictionary*)[allVideos objectAtIndex:k];
                DisplayImageView.myFrame=DisplayImageView.frame;
                //if (![[[allVideos objectAtIndex:k] valueForKey:@"isImage"] boolValue]){
                     DisplayImageView.image = (UIImage*)[[allVideos objectAtIndex:k] valueForKey:@"image"];
                    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                          initWithTarget:self action:@selector(albumselected:)];
                    lpgr.minimumPressDuration = 1.0; //seconds
                    [DisplayImageView addGestureRecognizer:lpgr];
                    [cell addSubview:DisplayImageView];
                   
                 
                /*}else{
                    NSURL * imageUrl = (NSURL*)[[allVideos objectAtIndex:k] valueForKey:@"url"];
                    
                    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
                    {
                        ALAssetRepresentation *rep = [myasset defaultRepresentation];
                        CGImageRef iref = [rep fullResolutionImage];
                        if (iref) {
                            UIImage *largeimage = [UIImage imageWithCGImage:iref];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                UIImage *scaledimg_pic=[[largeimage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(100, 100) interpolationQuality:0] fixOrientation];

                                DisplayImageView.image = scaledimg_pic;
                                UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                                      initWithTarget:self action:@selector(albumselected:)];
                                lpgr.minimumPressDuration = 1.0; //seconds
                                [DisplayImageView addGestureRecognizer:lpgr];

                                [cell addSubview:DisplayImageView];
                                
                            });
                            
                        } else {
                            NSLog(@"Error in resultblock in PhotoAlbumViewController!");
                        }
                    };
                    
                    // failure block
                    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
                    {
                        NSLog(@"Can't get image - %@",[myerror localizedDescription]);
                    };
                    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
                    [assetslibrary assetForURL:imageUrl
                                   resultBlock:resultblock
                                  failureBlock:failureblock];
                    
                }*/
               

            }

        }
   
    }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
       return cell;
    }
    }
}

- (void) albumselected:(UILongPressGestureRecognizer*)gesture
{
    CGPoint point = [gesture locationInView:self.view];
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
       
        MyImageView *tempVw=(MyImageView*)[gesture view];
        imageBeingDragged.frame=CGRectMake(tempVw.frame.origin.x, tempVw.frame.origin.y, tempVw.frame.size.width, tempVw.frame.size.height);
        imageBeingDragged.image=tempVw.image;
        imageBeingDragged.dict=tempVw.dict;
        NSLog(@"Selected Imageview Dic %@",imageBeingDragged.dict);

        [self animateFirstTouch:imageBeingDragged withLocation:point];
       
        imageBeingDragged.hidden=NO;
    }
    else if (gesture.state == UIGestureRecognizerStateChanged)
    {
        imageBeingDragged.center = point;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded     ||
             gesture.state == UIGestureRecognizerStateCancelled ||
             gesture.state == UIGestureRecognizerStateFailed)
    {
        // Determine if dragged view is in an OK drop zone
        // If so, then do the drop action, if not, return it to original location
        if (CGRectContainsPoint(tableview_filmstrip.frame, point)){
                [allFilmStrips addObject:imageBeingDragged.dict];
                [tableview_filmstrip reloadData];
                imageBeingDragged.hidden=YES;
                [self replaceSubview:tableview_filmstrip
                     withSubview:tableview_filmstrip
                      transition:kCATransitionFade
                       direction:kCATransitionFromTop
                        duration:0.3];
            
        }
        else{
            imageBeingDragged.hidden=YES;
            
        }
    }
}
-(void)replaceSubview:(UIView *)oldView withSubview:(UIView *)newView transition:(NSString *)transition direction:(NSString *)direction duration:(NSTimeInterval)duration
{
    // Set up the animation
    CATransition *animation = [CATransition animation];
    // Set the type and if appropriate direction of the transition,
    if (transition == kCATransitionFade)
    {
        [animation setType:kCATransitionFade];
    }
    else
    {
        [animation setType:transition];
        [animation setSubtype:direction];
    }
    // Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
    [animation setDuration:duration];
    [animation setTimingFunction:[CAMediaTimingFunction   functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[oldView layer] addAnimation:animation forKey:@"transitionViewAnimation"];
}
-(void) animateFirstTouch:(UIImageView *)image withLocation:(CGPoint)location {
    image.center = location;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    image.transform = CGAffineTransformMakeScale(1.5, 1.5);
    [UIView commitAnimations];
}

- (void) switchfilmstrip:(UILongPressGestureRecognizer*)gesture
{
    //@autoreleasepool {
    //tempImageView=nil;
    if (!tempImageView) {
        tempImageView=[[MyImageView alloc]init];
    }
    
    CGPoint point = [gesture locationInView:self.view];
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        MyImageView *selectedImageView=(MyImageView*)[gesture view];
        tempImageView=(MyImageView*)selectedImageView;
        
        
        [self animateSecondTouch:tempImageView withLocation:point];
        viewTrash.hidden=NO;
    }
    else if (gesture.state == UIGestureRecognizerStateChanged)
    {
        tempImageView.center = point;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded     ||
             gesture.state == UIGestureRecognizerStateCancelled ||
             gesture.state == UIGestureRecognizerStateFailed)
    {
        // Determine if dragged view is in an OK drop zone
        // If so, then do the drop action, if not, return it to original location
        viewTrash.hidden=TRUE;
        if (CGRectContainsPoint(tableview_filmstrip.frame, point)){
            NSIndexPath *indexPah=[NSIndexPath indexPathForRow:0 inSection:0];
            UITableViewCell *cell=[tableview_filmstrip cellForRowAtIndexPath:indexPah];
            BOOL isAnyMatch=FALSE;
            UIScrollView *scrollview = (UIScrollView*)[cell.contentView viewWithTag:1];
            MyImageView *movedImageView=(MyImageView*)[gesture view];
            for (UIView *imageview in scrollview.subviews)
            {
                if ([imageview isKindOfClass:[MyImageView class]]) {
                    
                    //if(CGRectIntersectsRect(movedImageView.frame, imageview.frame)){
                    if (CGRectContainsPoint(movedImageView.frame, imageview.center)){
                        
                        //we found the finally touched view
                        
                        MyImageView *matchImage=(MyImageView*)imageview;
                        if (![matchImage.dict isEqual:tempImageView.dict]) {
                            NSLog(@"Yeah !, i found it %@",matchImage.dict);
                            int matchindex=[allFilmStrips indexOfObject:matchImage.dict];
                            int movedindex=[allFilmStrips indexOfObject:tempImageView.dict];
                            [allFilmStrips removeObjectAtIndex:matchindex];
                            [allFilmStrips insertObject:tempImageView.dict atIndex:matchindex];
                            
                            [allFilmStrips removeObjectAtIndex:movedindex];
                            [allFilmStrips insertObject:matchImage.dict atIndex:movedindex];
                            [tableview_filmstrip reloadData];
                            [self replaceSubview:tableview_filmstrip
                                     withSubview:tableview_filmstrip
                                      transition:kCATransition
                                       direction:kCATransition
                                        duration:0.3];
                            
                            
                        }
                    }
                }
                
            }
            
            if (!isAnyMatch) {
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    tempImageView.frame = tempImageView.myFrame;
                    
                }completion:nil];
            }
            
            
        }
        else if (CGRectContainsPoint(viewTrash.frame, point)){
            
            [allFilmStrips removeObject:tempImageView.dict];
            [tableview_filmstrip reloadData];
            [self replaceSubview:tableview_filmstrip
                     withSubview:tableview_filmstrip
                      transition:kCATransition
                       direction:kCATransition
                        duration:0.3];
        }
        else{
            
            [UIView animateWithDuration:0.3 animations:^{
                
                tempImageView.frame = tempImageView.myFrame;
                
            }completion:nil];
            
        }
    }
    //}
    
    
}
-(void) animateSecondTouch:(UIImageView *)image withLocation:(CGPoint)location {
    image.center = location;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    image.transform = CGAffineTransformMakeScale(1, 1);
    [UIView commitAnimations];
}


- (IBAction)buttonPlay_Click:(id)sender {
    if (!arrayVideoPaths) {
        arrayVideoPaths=[[NSMutableArray alloc]initWithCapacity:allVideos.count];
    }
    else
        [arrayVideoPaths removeAllObjects];
    for (int j=0;j<allVideos.count;j++) {
        [arrayVideoPaths addObject:[NSString stringWithFormat:@"%d",j]];
    }
    
    for (int i=0;i<allVideos.count;i++) {
        NSMutableDictionary *dictemp=[allVideos objectAtIndex:i];
        if ([[dictemp valueForKey:@"isImage"] boolValue])
        {
          //Image in filmstrip -->make video of it
            counter=i;
            UIImage *largeimage = (UIImage*)[dictemp valueForKey:@"image"];
            UIImage *scaledimg_pic=[[largeimage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(400, 200) interpolationQuality:0] fixOrientation];
            [self converImageinvideo:scaledimg_pic Name:(NSString*)[NSString stringWithFormat:@"%d",counter]];
          /* NSURL *ImageUrl = [dictemp valueForKey:@"url"];
          
           // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                // result block
              
                ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
                {

                    ALAssetRepresentation *rep = [myasset defaultRepresentation];
                    CGImageRef iref = [rep fullResolutionImage];
                    if (iref) {
                        counter=i;

                        UIImage *largeimage = [UIImage imageWithCGImage:iref];
                        UIImage *scaledimg_pic=[[largeimage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(400, 200) interpolationQuality:0] fixOrientation];
                     
                        [self converImageinvideo:scaledimg_pic Name:(NSString*)[NSString stringWithFormat:@"%d",counter]];
                        
                    } else {
                        
                        
                    }
                    
                };
                
                // failure block
                ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
                {
                    NSLog(@"Can't get image - %@",[myerror localizedDescription]);
                };
                ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
                [assetslibrary assetForURL:ImageUrl
                               resultBlock:resultblock
                              failureBlock:failureblock];
                
            */
        
        }
        else{
            NSURL *videoUrl=(NSURL*)[dictemp valueForKey:@"url"];
            //NSString *videoBundleURL1 = [[NSBundle mainBundle] pathForResource:@"v2" ofType:@"mp4"];
            //NSURL *urlString = [NSURL fileURLWithPath:videoBundleURL1];
            [arrayVideoPaths replaceObjectAtIndex:i withObject:videoUrl];
            
        }
        if (i==allVideos.count-1) {
            //last object-->combine all
            [TrimVideoClass makeFilm:arrayVideoPaths];
        }
    }
}


-(void)converImageinvideo:(UIImage*)imagetoconvert Name:(NSString*)outputName
{
    
    ///////////// setup OR function def if we move this to a separate function ////////////
    // this should be moved to its own function, that can take an imageArray, videoOutputPath, etc...
    //    - (void)exportImages:(NSMutableArray *)imageArray
    // asVideoToPath:(NSString *)videoOutputPath
    // withFrameSize:(CGSize)imageSize
    // framesPerSecond:(NSUInteger)fps {
    
    NSError *error = nil;
    
    
    // set up file manager, and file videoOutputPath, remove "test_output.mp4" if it exists...
    //NSString *videoOutputPath = @"/Users/someuser/Desktop/test_output.mp4";
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory()
                                    stringByAppendingPathComponent:@"Documents"];
    
    NSString *videoOutputPath =[NSString stringWithFormat:@"%@.mp4",outputName];
    videoOutputPath=[documentsDirectory stringByAppendingPathComponent:videoOutputPath];
    //NSLog(@"-->videoOutputPath= %@", videoOutputPath);
    // get rid of existing mp4 if exists...
    if ([fileMgr removeItemAtPath:videoOutputPath error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
    
    
    CGSize imageSize = CGSizeMake(400, 200);
    NSUInteger fps = 30;
    
   
    NSMutableArray *imageArray=[[NSMutableArray alloc]init];
    [imageArray addObject:imagetoconvert];
    //[imageArray addObject:imagetoconvert];
    //////////////     end setup    ///////////////////////////////////
    
    NSLog(@"Start building video from defined frames.");
    
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:
                                  [NSURL fileURLWithPath:videoOutputPath] fileType:AVFileTypeQuickTimeMovie
                                                              error:&error];
    NSParameterAssert(videoWriter);
    
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:imageSize.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:imageSize.height], AVVideoHeightKey,
                                   nil];
    
    AVAssetWriterInput* videoWriterInput = [AVAssetWriterInput
                                            assetWriterInputWithMediaType:AVMediaTypeVideo
                                            outputSettings:videoSettings];
    
    
    AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor
                                                     assetWriterInputPixelBufferAdaptorWithAssetWriterInput:videoWriterInput
                                                     sourcePixelBufferAttributes:nil];
    
    NSParameterAssert(videoWriterInput);
    NSParameterAssert([videoWriter canAddInput:videoWriterInput]);
    videoWriterInput.expectsMediaDataInRealTime = YES;
    [videoWriter addInput:videoWriterInput];
    
    //Start a session:
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    CVPixelBufferRef buffer = NULL;
    
    //convert uiimage to CGImage.
    int frameCount = 0;
    double numberOfSecondsPerFrame = 1;
    double frameDuration = fps * numberOfSecondsPerFrame;
    
    //for(VideoFrame * frm in imageArray)
    NSLog(@"**************************************************");
    for(UIImage * img in imageArray)
    {
        //UIImage * img = frm._imageFrame;
        buffer = [self pixelBufferFromCGImage:[img CGImage]];
        
        BOOL append_ok = NO;
        int j = 0;
        while (!append_ok && j < 30) {
            if (adaptor.assetWriterInput.readyForMoreMediaData)  {
                //print out status:
                NSLog(@"Processing video frame (%d,%d)",frameCount,[imageArray count]);
                
                CMTime frameTime = CMTimeMake(frameCount*frameDuration,(int32_t) fps);
                append_ok = [adaptor appendPixelBuffer:buffer withPresentationTime:frameTime];
                if(!append_ok){
                    NSError *error = videoWriter.error;
                    if(error!=nil) {
                        NSLog(@"Unresolved error %@,%@.", error, [error userInfo]);
                    }
                }
            }
            else {
                printf("adaptor not ready %d, %d\n", frameCount, j);
                [NSThread sleepForTimeInterval:0.1];
            }
            j++;
        }
        if (!append_ok) {
            printf("error appending image %d times %d\n, with error.", frameCount, j);
        }
        frameCount++;
    }
    NSLog(@"**************************************************");
    
    //Finish the session:
    [videoWriterInput markAsFinished];
    [videoWriter finishWriting];
    NSLog(@"Write Ended");
    NSURL *urltosave=[NSURL fileURLWithPath:videoOutputPath];
    [arrayVideoPaths replaceObjectAtIndex:counter withObject:urltosave];

    /*
    
    ////////////////////////////////////////////////////////////////////////////
    //////////////  OK now add an audio file to move file  /////////////////////
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    NSString *bundleDirectory = [[NSBundle mainBundle] bundlePath];
    // audio input file...
    NSString *audio_inputFilePath = [bundleDirectory stringByAppendingPathComponent:@"song.mp3"];
    NSURL    *audio_inputFileUrl = [NSURL fileURLWithPath:audio_inputFilePath];
    
    // this is the video file that was just written above, full path to file is in --> videoOutputPath
    NSURL    *video_inputFileUrl = [NSURL fileURLWithPath:videoOutputPath];
    
    // create the final video output file as MOV file - may need to be MP4, but this works so far...
    NSString *outputFilePath = [documentsDirectory stringByAppendingPathComponent:@"final_video.mp4"];
    NSURL    *outputFileUrl = [NSURL fileURLWithPath:outputFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
        [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
    
    CMTime nextClipStartTime = kCMTimeZero;
    
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:video_inputFileUrl options:nil];
    CMTimeRange video_timeRange = CMTimeRangeMake(kCMTimeZero,videoAsset.duration);
    AVMutableCompositionTrack *a_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    [a_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:nextClipStartTime error:nil];
    
    //nextClipStartTime = CMTimeAdd(nextClipStartTime, a_timeRange.duration);
    
    AVURLAsset* audioAsset = [[AVURLAsset alloc]initWithURL:audio_inputFileUrl options:nil];
    CMTimeRange audio_timeRange = CMTimeRangeMake(kCMTimeZero, audioAsset.duration);
    AVMutableCompositionTrack *b_compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    [b_compositionAudioTrack insertTimeRange:audio_timeRange ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:nextClipStartTime error:nil];
    
    
    
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    //_assetExport.outputFileType = @"com.apple.quicktime-movie";
    _assetExport.outputFileType = @"public.mpeg-4";
    //NSLog(@"support file types= %@", [_assetExport supportedFileTypes]);
    _assetExport.outputURL = outputFileUrl;
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         //[self saveVideoToAlbum:outputFilePath];
     }
     ];
    
    ///// THAT IS IT DONE... the final video file will be written here...
    NSLog(@"DONE.....outputFilePath--->%@", outputFilePath);
    
    // the final video file will be located somewhere like here:
    // /Users/caferrara/Library/Application Support/iPhone Simulator/6.0/Applications/D4B12FEE-E09C-4B12-B772-7F1BD6011BE1/Documents/outputFile.mov
    
    */
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
}


////////////////////////
- (CVPixelBufferRef) pixelBufferFromCGImage: (CGImageRef) image {
    
    
    CGSize size = CGSizeMake(400, 200);
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault,
                                          size.width,
                                          size.height,
                                          kCVPixelFormatType_32ARGB,
                                          (__bridge CFDictionaryRef) options,
                                          &pxbuffer);
    if (status != kCVReturnSuccess){
        NSLog(@"Failed to create pixel buffer");
    }
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, size.width,
                                                 size.height, 8, 4*size.width, rgbColorSpace,
                                                 kCGImageAlphaPremultipliedFirst);
    //kCGImageAlphaNoneSkipFirst);
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image),
                                           CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}
////////////////////////

@end
