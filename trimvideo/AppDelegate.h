//
//  AppDelegate.h
//  trimvideo
//
//  Created by Anuj on 6/2/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
