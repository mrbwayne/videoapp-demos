//
//  MyImageView.h
//  trimvideo
//
//  Created by Anuj on 6/11/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyImageView : UIImageView
@property(nonatomic,retain)NSMutableDictionary *dict;
@property(assign)CGRect myFrame;

@end
