//
//  YoutubeViewController.m
//  trimvideo
//
//  Created by Anuj on 6/16/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import "YoutubeViewController.h"
#define YOUTUBE_APP_KEY @"AIzaSyB4z2j0jjcc0KNu-9eiMcz8QgKjmdukmz8"
//#define YOUTUBE_APP_KEY @"AIzaSyA2S1SeC8uX968bI3-1JWkNC2GG6GEFIpA"

@interface YoutubeViewController ()

@end

@implementation YoutubeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableview Delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (tableView != self.searchDisplayController.searchResultsTableView) {
        
        return arrayYoutubeVideos.count;
    }else
    {
       return arraySearchedVideo.count;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int row = 0;
    
    if (tableView != self.searchDisplayController.searchResultsTableView) {

        row = 90.0;
    }
    else{
        row = 90.0;
    }
    
    return row;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    //NSMutableArray *messagesOfDay = [arrayYoutubeVideos objectAtIndex:indexPath.row];
    //NSMutableDictionary *s = (NSMutableDictionary *) [messagesOfDay objectAtIndex:indexPath.row];
    
    NSDictionary *dic = [arraySearchedVideo objectAtIndex:indexPath.row];
    
    NSString *cellIdentifier;
    if (tableView != self.searchDisplayController.searchResultsTableView) {

        cellIdentifier = [NSString stringWithFormat:@"Youtube%d%d",indexPath.section,indexPath.row];
    }else
    {
        cellIdentifier = [NSString stringWithFormat:@"Search%d%d%@",indexPath.section,indexPath.row,[[dic objectForKey:@"snippet"]valueForKey:@"title"]];
    }
    UITableViewCell * cell   = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //cell=nil;
    if (!cell)
    {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        [cell setBackgroundColor:[UIColor whiteColor]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        
        /*
        NSLog(@"dic :: %@",dic);
        cell.textLabel.text = [[dic objectForKey:@"snippet"]valueForKey:@"title"];

      
      
       
       
        NSString *dateString =[[dic objectForKey:@"snippet"] valueForKey:@"publishedAt"];
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate* date = [df dateFromString:[dateString stringByReplacingOccurrencesOfString:@".000Z" withString:@"-0000"]];
        [df setDateFormat:@"MMM-dd-yyyy"];
        NSString *result = [df stringFromDate:date];
        
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",result];
        */
        NSString *strImage=[[[[[dic objectForKey:@"snippet" ]objectForKey:@"thumbnails"] objectForKey:@"medium"]valueForKey:@"url"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *imgUrl =  [NSURL URLWithString:strImage];
        NSLog(@"imgUrl :: %@",imgUrl);
        //load the image
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSURL *url=[NSURL URLWithString:strImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *img = [[UIImage alloc] initWithData:data];
            // [buttonUserImage setImage:img forState:UIControlStateNormal];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"GOT IMAGE");
                cell.imageView.image=img;
                
            });
            
            
        });
     
        cell.textLabel.text = [[dic objectForKey:@"snippet"]valueForKey:@"title"];
        NSString *strViewCount=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"statistics"]valueForKey:@"viewCount"]];
        
        if (strViewCount)
        {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[self returnCommaSeperatedDigit:strViewCount]];
        }
        else
        {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"0"];
        }
        //NSString *strDur=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"contentDetails"]valueForKey:@"duration"]];
        //cell.detailTextLabel.text=strDur;
        
    }
    return cell;
}
#pragma mark - Comma Digit
-(NSString *)returnCommaSeperatedDigit :(NSString *)strDigit
{
    // ***********************************************
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithInteger:[strDigit integerValue]]];
    return formatted;
    // ***********************************************
}
#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView )
    {
          /*  NSDictionary *dic ;
            dic = [arraySearchedVideo objectAtIndex:indexPath.row];
            
            NSString *videoId = [NSString stringWithFormat:@"%@",[dic valueForKey:@"id"]];
            // NSString *videoId = [NSString stringWithFormat:@"nGyQuZo9tIQ"];
            PBYouTubeVideoViewController *viewController = [[PBYouTubeVideoViewController alloc] initWithVideoId:videoId];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            [self presentViewController:navigationController animated:YES completion:NULL];
        */
        [self Showhud:@"Downloading..."];
        //-----------------------------------------------------------------------------------------------------
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:[[arraySearchedVideo objectAtIndex:indexPath.row] valueForKey:@"id"]];
        videoPlayerViewController.delegate=self;
        videoPlayerViewController.isOnlyNeedVideoDownloadLink=TRUE;
        videoPlayerViewController.preferredVideoQualities = [self getTheVideoQualityFromSettings];
        //-----------------------------------------------------------------------------------------------------

        
    }
}
-(void)getDownloadLink :(NSURL *) urlDownloadLink{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    documentsPath=[documentsPath stringByAppendingString:@"/Donwloaded"];
    
    outputURL = documentsPath;
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    outputURL = [outputURL stringByAppendingPathComponent:@"film.mp4"];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    
    
   
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:urlDownloadLink];
    [request setDownloadDestinationPath:outputURL];
    [request setDownloadProgressDelegate:hud];
    request.allowResumeForFileDownloads=TRUE;
    request.tag=5050;
    [request setTimeOutSeconds:100000];
    [request setDelegate:self];
    [request startAsynchronous];
    
}

-(NSArray *)getTheVideoQualityFromSettings
{
    NSArray *arrVideoQuality;
    // NSString *strVideoQuality =  [[NSUserDefaults standardUserDefaults] valueForKey:@"videoQuality"];
    NSString *strVideoQuality =  @"240";
    if ([strVideoQuality isEqualToString:@"37"]) // For HD
    {
        arrVideoQuality=  @[ XCDYouTubeVideoQualityHTTPLiveStreaming, @(XCDYouTubeVideoQualityHD720), @(XCDYouTubeVideoQualityMedium360), @(XCDYouTubeVideoQualitySmall240) ];
    }
    else if ([strVideoQuality isEqualToString:@"22"]) // For 720 HD videos
    {
        
        arrVideoQuality=  @[ @(XCDYouTubeVideoQualityHD720), XCDYouTubeVideoQualityHTTPLiveStreaming, @(XCDYouTubeVideoQualityMedium360), @(XCDYouTubeVideoQualitySmall240) ];
    }
    else if ([strVideoQuality isEqualToString:@"18"]) //For 360 p
    {
        
        arrVideoQuality=  @[ @(XCDYouTubeVideoQualityMedium360), @(XCDYouTubeVideoQualitySmall240),@(XCDYouTubeVideoQualityHD720), XCDYouTubeVideoQualityHTTPLiveStreaming ];
    }
    else if ([strVideoQuality isEqualToString:@"36"]) // For 240 p
    {
        
        arrVideoQuality=  @[ @(XCDYouTubeVideoQualitySmall240), @(XCDYouTubeVideoQualityMedium360),@(XCDYouTubeVideoQualityHD720), XCDYouTubeVideoQualityHTTPLiveStreaming ];
    }
    else
    {
        arrVideoQuality=  @[ XCDYouTubeVideoQualityHTTPLiveStreaming, @(XCDYouTubeVideoQualityHD720), @(XCDYouTubeVideoQualityMedium360), @(XCDYouTubeVideoQualitySmall240) ];
        
    }
    return arrVideoQuality;
}
#pragma mark Content Filtering
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

    [self getYoutubeVideoWithFilters:@"rating" strSearchValue:searchBar.text LoadMoreData:NO];
    //[self getYoutubeVideoPlaylists:@"rating" :searchBar.text AddType:@"channels" LoadMoreData:NO];

}
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    
    [arraySearchedVideo removeAllObjects];
    
}


#pragma mark - UISearchDisplayController Delegate Methods

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    
    [tableYoutubeVideos reloadData];
    
}
-(void)getYoutubeVideoWithFilters:(NSString *)strOrderBy strSearchValue:(NSString *)strSearch LoadMoreData :(BOOL) isLoadMore
{
        NSString *escapedUrlString;
        NSString *regioncode = @"IN";
        
        NSString *strVideoDuration =@"any";
        // VideoType - Any,Movie,Searial
        NSString *strVideoType =@"any";
        //publishedAfter
        NSString *strPublishedDate=@"2000-01-01T00:00:00Z";
        
        if (isLoadMore)
        {
            NSString *strNextPageToken=[[NSUserDefaults standardUserDefaults]objectForKey:@"NextPageToken"];
            
            if (strNextPageToken)
            {
                // old One
                //           escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=15&order=%@&q=%@&safeSearch=strict&type=video&videoDuration=any&key=%@&pageToken=%@",strOrderBy,strSearch,YOUTUBE_APP_KEY,strNextPageToken];
                
                escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=15&order=%@&q=%@&type=video&key=%@&pageToken=%@&regionCode=%@&safeSearch=moderate&videoDuration=%@&videoType=%@&publishedAfter=%@",strOrderBy,strSearch,YOUTUBE_APP_KEY,strNextPageToken,regioncode,strVideoDuration,strVideoType,strPublishedDate];
            }
        }
        else
        {
            // filterVideoDate
            escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=15&order=%@&q=%@&type=video&key=%@&regionCode=%@&safeSearch=moderate&videoDuration=%@&videoType=%@&publishedAfter=%@",strOrderBy,strSearch,YOUTUBE_APP_KEY,regioncode,strVideoDuration,strVideoType,strPublishedDate];
            NSLog(@"escapedUrlString : %@",escapedUrlString);
        }
        
        NSURL *url = [NSURL URLWithString:[escapedUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setTag:100];
        [request setTimeOutSeconds:200];
        [request setRequestMethod:@"GET"];
        [request setDelegate:self];
        [request startAsynchronous];
}
-(void)getYoutubeVideoPlaylists :(NSString *)strOrderBy :(NSString *)strSearchText AddType:(NSString *)strType LoadMoreData :(BOOL) isLoadMore
{
    
        // Type ---- 1) playlist
        // Tyoe ---- 2) channel
        NSString *escapedUrlString;
        if (isLoadMore)
        {
            NSString *strNextPageToken=[[NSUserDefaults standardUserDefaults]objectForKey:@"NextPageToken"];
            
            if (strNextPageToken)
            {
                escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=50&order=%@&q=%@&type=%@&key=%@&pageToken=%@",strOrderBy,strSearchText,strType,YOUTUBE_APP_KEY,strNextPageToken];
            }
        }
        else
        {
            escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=50&order=%@&q=%@&type=%@&key=%@",strOrderBy,strSearchText,strType,YOUTUBE_APP_KEY];
        }
        
        NSLog(@"escapedUrlString :: %@",escapedUrlString);
        NSURL *url = [NSURL URLWithString:[escapedUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setTag:700];
        [request setTimeOutSeconds:200];
        [request setRequestMethod:@"GET"];
        [request setDelegate:self];
        [request startAsynchronous];
        
}
#pragma mark -  ASIHTTP Methods

-(void)requestStart:(ASIHTTPRequest *)request
{
    //   AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    // [appDelegate showHUD];
    
}
- (void)requestDone:(ASIHTTPRequest *)request
{
    // NSString *response = [request responseString];
    // AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //[appDelegate hideHUD];
    
}

- (void)requestWentWrong:(ASIHTTPRequest *)request
{
    //  NSError *error = [request error];
    //  AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    // [appDelegate hideHUD];
    
}
#pragma mark Req Finished

-(void)requestFinished:(ASIHTTPRequest *)request
{
    if (request.tag == 100)
    {
        NSString *responseString = [request responseString];
        NSDictionary *responseDic = (NSDictionary*)[responseString JSONValue];
        
        NSString *strNextPageToken=[responseDic valueForKey:@"nextPageToken"];
        
        NSLog(@"strNextPageToken : %@",strNextPageToken);
        NSLog(@"KEY FROM DB : %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NextPageToken"]);
        
        [[NSUserDefaults standardUserDefaults]setObject:strNextPageToken forKey:@"NextPageToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (responseDic)
        {
            NSMutableArray *arrVideoDetails = [responseDic objectForKey:@"items"];
            responseDic =nil;
            NSLog(@"arrVideoDetails :: %@",arrVideoDetails);
            [self searchVideosFromIds:arrVideoDetails ForPlaylist:0];
        }
        else
        {
            UIAlertView *alertViewForReachability=[[UIAlertView alloc]initWithTitle:@"Internet Error" message:@"Internet Connection not available currently. Please check your internet connectivity and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertViewForReachability show];
            
        }
    }
    else if (request.tag==200)
    {
        
        NSString *responseString = [request responseString];
        NSMutableDictionary *responseDic = (NSMutableDictionary*)[responseString JSONValue];
        
        if (responseDic)
        {
            NSLog(@"responsedic>>>%@",responseDic.description);
            [arraySearchedVideo removeAllObjects];
             arraySearchedVideo=[responseDic objectForKey:@"items"];
             [self.searchDisplayController.searchResultsTableView reloadData];
        }
        else
        {
            
        }
        
        //[appDelegate hideHUD];
        
    }
    else if (request.tag==700)
    {
        
        NSString *responseString = [request responseString];
        NSDictionary *responseDic = (NSDictionary*)[responseString JSONValue];
        
        NSString *strNextPageToken=[responseDic valueForKey:@"nextPageToken"];
        
        [[NSUserDefaults standardUserDefaults]setObject:strNextPageToken forKey:@"NextPageToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (responseDic)
        {
            NSMutableArray *arrVideoDetails = [responseDic objectForKey:@"items"];
            responseDic =nil;
            NSLog(@"arrVideoDetails :: %@",arrVideoDetails);
            [self searchVideosFromPlaylists:arrVideoDetails];
        }
        else
        {
            
        }
        
        
    }
    else if (request.tag==550)
    {
        
        
        NSString *responseString = [request responseString];
        NSMutableDictionary *responseDic = (NSMutableDictionary*)[responseString JSONValue];
        
        NSString *strNextPageToken=[responseDic valueForKey:@"nextPageToken"];
        
        NSLog(@"strNextPageToken : %@",strNextPageToken);
        NSLog(@"KEY FROM DB : %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NextPageToken"]);
        
        //   [[NSUserDefaults standardUserDefaults]setObject:strNextPageToken forKey:@"NextPageToken"];
        //   [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        if (responseDic)
        {
            NSLog(@"arrVideoDetails :: %@",responseDic.description);
            NSMutableArray *temparr =[responseDic objectForKey:@"items"];
           
            
            NSString *strChannelID=@"";
            for (int i =0; i<temparr.count; i++) {
                if(i==temparr.count-1)
                    strChannelID = [strChannelID stringByAppendingString:[NSString stringWithFormat:@"%@", [[temparr objectAtIndex:i] valueForKey:@"id"]]];
                else
                   strChannelID = [strChannelID stringByAppendingString:[NSString stringWithFormat:@"%@,", [[temparr objectAtIndex:i] valueForKey:@"id"]]];
            }
            if ([strChannelID isEqualToString:@""]||strChannelID.length==0)
            {
                
            }
            else{
               [self getListOfChannelVideos:strChannelID LoadMoreData:NO];
            }
            

        }
        else
        {
            
        }
        
        //[appDelegate hideHUD];
        
    }
    else if (request.tag==900)
    {
        // 555555555
        NSString *responseString = [request responseString];
        NSMutableDictionary *responseDic = (NSMutableDictionary*)[responseString JSONValue];
        if (responseDic)
        {
            NSString *strNextPageToken=[responseDic valueForKey:@"nextPageToken"];
            [[NSUserDefaults standardUserDefaults]setObject:strNextPageToken forKey:@"NextPageToken"];
            NSMutableArray *arrVideoDetails = [responseDic objectForKey:@"items"];
            responseDic =nil;
            NSLog(@"arrVideoDetails :: %@",arrVideoDetails);
            [self searchVideosFromIds:arrVideoDetails ForPlaylist:2];
        }
        else
        {
            UIAlertView *alertViewForReachability=[[UIAlertView alloc]initWithTitle:@"Internet Error" message:@"Internet Connection not available currently. Please check your internet connectivity and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertViewForReachability show];
        }
        
        
        
    }
    else if (request.tag==1000){
    
        
            NSString *responseString = [request responseString];
            NSMutableDictionary *responseDic = (NSMutableDictionary*)[responseString JSONValue];
            
            // 55555555
            // from Playlist
            if (responseDic)
            {
                NSString *strNextPageToken=[responseDic valueForKey:@"nextPageToken"];
                [[NSUserDefaults standardUserDefaults]setObject:strNextPageToken forKey:@"NextPageToken"];
                
                //            if ([delegate respondsToSelector:@selector(getListOfPlaylistItems:)]) {
                //                [delegate getListOfPlaylistItems:responseDic];
                //            }
                
                NSMutableArray *arrVideoDetails = [responseDic objectForKey:@"items"];
                
                responseDic =nil;
                
                NSLog(@"arrVideoDetails :: %@",arrVideoDetails);
                
                [self searchVideosFromIds:arrVideoDetails ForPlaylist:1];
            }
            else
            {
                
                UIAlertView *alertViewForReachability=[[UIAlertView alloc]initWithTitle:@"Internet Error" message:@"Internet Connection not available currently. Please check your internet connectivity and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertViewForReachability show];
                
            }
            // [appDelegate hideHUD];
    }
    else if (request.tag==5050)
    {
        [self hideHud];
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:outputURL] completionBlock:^(NSURL *assetURL, NSError *error) {
            if (error) {
                NSLog(@"error>%@",error.description);
            }
            else{
                NSLog(@"asseturl>%@",assetURL);
                 [[NSFileManager defaultManager] removeItemAtPath:outputURL error:nil];
            
            }
        }];
        [[[UIAlertView alloc] initWithTitle:@"Downloading"
                                    message:@"Success!!!"
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil]  show];

        //[appDelegate hideHUD];
        
    }
    
}
-(void)requestFailed:(ASIHTTPRequest *)request
{
    //AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
-(void)getListOfChannelVideos : (NSString *)channelID LoadMoreData :(BOOL) isLoadMore
{
        NSString *escapedUrlString;
        if (isLoadMore)
        {
            NSString *strNextPageToken=[[NSUserDefaults standardUserDefaults]objectForKey:@"NextPageToken"];
            if (strNextPageToken)
            {
                escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?&part=id&channelId=%@&maxResults=50&key=%@&pageToken=%@",channelID,YOUTUBE_APP_KEY,strNextPageToken];
                
                NSLog(@"escapedUrlString : %@",escapedUrlString);
            }
        }
        else
        {
            //https://www.googleapis.com/youtube/v3/search?&part=id,snippet&channelId=UCugb_j1Et8HRUpGiboLsPCw&maxResults=50&key=%@
            
            escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?&part=id&channelId=%@&maxResults=50&key=%@",channelID,YOUTUBE_APP_KEY];
            
            NSLog(@"escapedUrlString : %@",escapedUrlString);
            
        }
        
        NSURL *url = [NSURL URLWithString:[escapedUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setTag:900];
        [request setTimeOutSeconds:200];
        [request setRequestMethod:@"GET"];
        [request setDelegate:self];
        [request startAsynchronous];
   
}
-(void)searchVideosFromPlaylists :(NSMutableArray *) arrVideoId
{
    // availabel Items
    //id,status,snippet,contentDetails,player
    
    //   https://www.googleapis.com/youtube/v3/playlists?part=id,snippet,contentDetails&id=PLZaE0f1D7EdZUU4YIE1ps_pA8x81xQGGv,PLZaE0f1D7EdbHnlzRknCshW1ouxfF2JMS,PLZaE0f1D7EdbHnlzRknCshW1ouxfF2JMS&maxResults=10&key=AIzaSyCdZwWsaQPNol_V9sZ-evZ1UJw8QfRTNVE
    
    NSString *strCategory;
    NSString *strKey;
    NSString *strStatistics;
    //    if ([appDelegate.strCategoryName isEqualToString:@"playlist"])
    //    {
    //        strCategory =@"playlists"; // api name
    //        strKey=@"playlistId";
    //        strStatistics=@"";
    //    }
    //    else
    //    {
    strCategory =@"channels"; // channels api
    strKey=@"channelId";
    strStatistics=@",statistics";
    //    }
    
    NSString *strCommaSeperatedValue;
    for (int i= 0; i<arrVideoId.count; i++)
    {
        NSString *strPartValue=[[[arrVideoId objectAtIndex:i]objectForKey:@"id"]valueForKey:strKey];
        
        if (strPartValue)
        {
            if (!strCommaSeperatedValue)
            {
                strCommaSeperatedValue=strPartValue;
            }
            else
            {
                strCommaSeperatedValue=[NSString stringWithFormat:@"%@,%@",strCommaSeperatedValue,strPartValue];
                
            }
        }
    }
    
    NSLog(@"strCommaSeperatedValue :: %@",strCommaSeperatedValue);
    
    NSString *escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/%@?part=id,snippet,contentDetails%@&id=%@&key=%@",strCategory,strStatistics,strCommaSeperatedValue,YOUTUBE_APP_KEY];
    
    NSLog(@"escapedUrlString : %@",escapedUrlString);
    
    NSURL *url = [NSURL URLWithString:[escapedUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setTag:550];
    [request setTimeOutSeconds:200];
    [request setRequestMethod:@"GET"];
    [request setDelegate:self];
    [request startAsynchronous];
    
    //https://www.googleapis.com/youtube/v3/channels?part=id,snippet,contentDetails,statistics&id=UC0ia0Bh33OgZ8p4emfeGG6g%2CUCcNMM8-MBjv7XbI_K-hVIvQ&key=AIzaSyCdZwWsaQPNol_V9sZ-evZ1UJw8QfRTNVE
    
}
-(void)searchVideosFromIds :(NSMutableArray *) arrVideoId ForPlaylist :(int)apiType
{
    // YES == 1
    // NO == 0
    
    //https://www.googleapis.com/youtube/v3/videos?id=sSHIPlteqP4&key=AIzaSyCdZwWsaQPNol_V9sZ-evZ1UJw8QfRTNVE&part=id,snippet,contentDetails,statistics
    
    NSString *strCommaSeperatedValue =[self returnCommaSeperatedVideoKey:arrVideoId :apiType];
    
    NSString *escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?id=%@&key=%@&part=id,snippet,contentDetails,statistics",strCommaSeperatedValue,YOUTUBE_APP_KEY];
    
    NSLog(@"escapedUrlString : %@",escapedUrlString);
    
    NSURL *url = [NSURL URLWithString:[escapedUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    if (apiType==1)
        [request setTag:200];
    else
         [request setTag:200];
    [request setTimeOutSeconds:200];
    [request setRequestMethod:@"GET"];
    [request setDelegate:self];
    [request startAsynchronous];
    
}
-(NSString *)returnCommaSeperatedVideoKey :(NSMutableArray *) arrVideoId  :(int)typeOfApi
{
    
    NSString *strCommaSeperatedValue;
    
    for (int i= 0; i<arrVideoId.count; i++)
    {
        NSString *strPartValue;
        if (typeOfApi==2)
        {
            if ([[[arrVideoId objectAtIndex:i]objectForKey:@"id"]valueForKey:@"videoId"])
            {
                strPartValue=[[[arrVideoId objectAtIndex:i]objectForKey:@"id"]valueForKey:@"videoId"];
            }
        }
        else if (typeOfApi==1)
        {
            if ([[[arrVideoId objectAtIndex:i]objectForKey:@"contentDetails"]valueForKey:@"videoId"])
            {
                strPartValue=[[[arrVideoId objectAtIndex:i]objectForKey:@"contentDetails"]valueForKey:@"videoId"];
            }
        }
        else
        {
            if ([[[arrVideoId objectAtIndex:i]objectForKey:@"id"] isKindOfClass:[NSDictionary class]])
            {
                strPartValue=[[[arrVideoId objectAtIndex:i]objectForKey:@"id"]valueForKey:@"videoId"];
            }
            else
            {
                strPartValue=[[arrVideoId objectAtIndex:i]valueForKey:@"id"];
            }
        }
        
        //****************************************************
        if (strPartValue)
        {
            if (!strCommaSeperatedValue)
            {
                strCommaSeperatedValue=strPartValue;
            }
            else
            {
                strCommaSeperatedValue=[NSString stringWithFormat:@"%@,%@",strCommaSeperatedValue,strPartValue];
            }
        }
        //****************************************************
        
    }
    
    NSLog(@"strCommaSeperatedValue :: %@",strCommaSeperatedValue);
    return strCommaSeperatedValue;
}

#pragma mark - on selection of video
-(void)getPlaylistItems :(NSString *)PlaylistID LoadMoreData :(BOOL) isLoadMore
{
   
        NSString *escapedUrlString;
        if (isLoadMore)
        {
            NSString *strNextPageToken=[[NSUserDefaults standardUserDefaults]objectForKey:@"NextPageToken"];
            if (strNextPageToken)
            {
                escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&playlistId=%@&key=%@&pageToken=%@",PlaylistID,YOUTUBE_APP_KEY,strNextPageToken];
                
                NSLog(@"@@@@@@ escapedUrlString : %@",escapedUrlString);
                
            }
        }
        else
        {
            //https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=15&playlistId=PLOrkKJBmKB7oO00_DZScbigzLGvkXpiE3&key=AIzaSyCdZwWsaQPNol_V9sZ-evZ1UJw8QfRTNVE
            
            escapedUrlString =[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&playlistId=%@&key=%@",PlaylistID,YOUTUBE_APP_KEY];
            
            NSLog(@"escapedUrlString : %@",escapedUrlString);
            
        }
        
        NSURL *url = [NSURL URLWithString:[escapedUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setTag:1000];
        [request setTimeOutSeconds:200];
        [request setRequestMethod:@"GET"];
        [request setDelegate:self];
        [request startAsynchronous];
   
}
#pragma mark - Hud method

-(void)Showhud:(NSString *)message {
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:hud];
	hud.mode = MBProgressHUDModeDeterminate;
	hud.delegate = self;
    [hud setLabelText:message];
    [hud show:TRUE];
    hud.dimBackground=TRUE;
    [hud setRemoveFromSuperViewOnHide:YES];
}
-(void)hideHud {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [hud setHidden:YES];
    
}
@end
