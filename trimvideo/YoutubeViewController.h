//
//  YoutubeViewController.h
//  trimvideo
//
//  Created by Anuj on 6/16/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSON.h"
#import "ASIHTTPRequest.h"
#import "PBYouTubeVideoViewController.h"
#import "XCDYouTubeKit.h"
#include "MBProgressHUD.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ASIProgressDelegate.h"

@interface YoutubeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ASIHTTPRequestDelegate,UISearchBarDelegate,UISearchDisplayDelegate,XCDYouTubeVideoPlayerClassDelegate,ASIProgressDelegate,MBProgressHUDDelegate>
{


    __weak IBOutlet UISearchBar *searchYoutube;

    __weak IBOutlet UITableView *tableYoutubeVideos;

    NSMutableArray *arrayYoutubeVideos;
    NSMutableArray *arraySearchedVideo;
    MBProgressHUD *hud;
    NSString *outputURL;
}
@end
