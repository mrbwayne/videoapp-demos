//
//  ViewController.m
//  trimvideo
//
//  Created by Anuj on 6/2/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize moviePlayerController;
@synthesize mainImage;
@synthesize tempDrawImage;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    red = 255.0/255.0;
    green = 0.0/255.0;
    blue = 0.0/255.0;
    brush = 10.0;
    opacity = 1.0;
    NSString *videoBundleURL1 = [[NSBundle mainBundle] pathForResource:@"v1" ofType:@"mp4"];
    NSString *videoBundleURL2 = [[NSBundle mainBundle] pathForResource:@"v2" ofType:@"mp4"];
    [TrimVideoClass trimVideo:[NSURL fileURLWithPath:videoBundleURL1] StartTimeInMinute:2 EndTimeInMinute:3];
    [TrimVideoClass mergeVideos:videoBundleURL1 Video2Path:videoBundleURL2 StartMinute:1 EndTimeInMinute:2];
    [self playvideo:videoBundleURL2];
    videoBundleURL1=nil;
    videoBundleURL2=nil;
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMix_Click:(id)sender {
    
    
    NSString *videoBundleURL2 = [[NSBundle mainBundle] pathForResource:@"v2" ofType:@"mp4"];
    [self MixVideoWithText:[NSURL fileURLWithPath:videoBundleURL2]];
    [self.moviePlayerController stop];
    [self.moviePlayerController.view removeFromSuperview];
    self.mainImage.hidden=TRUE;

}

-(void)MixVideoWithText:(NSURL*)url
{
    @autoreleasepool {
        
    
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:url options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAssetTrack *clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    //If you need audio as well add the Asset Track for audio here
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    
    [compositionVideoTrack setPreferredTransform:[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform]];
    
    CGSize sizeOfVideo=[clipVideoTrack naturalSize];
    //TextLayer defines the text they want to add in Video
    //Text of watermark
    /*
    CATextLayer *textOfvideo=[[CATextLayer alloc] init];
    textOfvideo.string=[NSString stringWithFormat:@"%@",@"TESTING"];//text is shows the text that you want add in video.
    [textOfvideo setFont:(__bridge CFTypeRef)([UIFont systemFontOfSize:12.0])];//fontUsed is the name of font
    [textOfvideo setFrame:CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height/6)];
    [textOfvideo setAlignmentMode:kCAAlignmentCenter];
    [textOfvideo setForegroundColor:[[UIColor blueColor] CGColor]];
    */
    
    /*
    CAShapeLayer *aLayer = [CALayer layer];
    aLayer.bounds = CGRectMake(0, 0, 110, 110);
    aLayer.position = CGPointMake(sizeOfVideo.width/2, sizeOfVideo.height/2);  // center it
    aLayer.contents = (__bridge id)[self drawTouchTrackerAndGetUIImage].CGImage;
    */

    //Image of watermark
    UIImage *myImage=self.mainImage.image;
    CALayer *layerCa = [CALayer layer];
    layerCa.contents = (id)myImage.CGImage;
    layerCa.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    layerCa.opacity = 1.0;
    layerCa.masksToBounds=YES;
    
    
    CALayer *parentLayer=[CALayer layer];
    CALayer *videoLayer=[CALayer layer];
    parentLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    videoLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:layerCa];
   
    AVMutableVideoComposition *videoComposition=[AVMutableVideoComposition videoComposition] ;
    videoComposition.frameDuration=CMTimeMake(1, 10);
    videoComposition.renderSize=sizeOfVideo;
    videoComposition.animationTool=[AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    

    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    exportSession.videoComposition=videoComposition;
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    documentsPath=[documentsPath stringByAppendingString:@"/OverlayVideos"];
    
    NSString *outputURL = documentsPath;
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    outputURL = [outputURL stringByAppendingPathComponent:@"overlay.mov"];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    NSLog(@"exportSession.supportedFileTypes>>>>%@",exportSession.supportedFileTypes);

    exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch (exportSession.status)
        {
            case AVAssetExportSessionStatusCompleted:
                NSLog(@"Export OK");
                self.mainImage.image = nil;
                self.mainImage.hidden=FALSE;
                //[self exportedvideoplay:outputURL];
                [self performSelectorOnMainThread:@selector(playvideo:) withObject:outputURL waitUntilDone:NO];
                break;
            case AVAssetExportSessionStatusFailed:
                NSLog (@"AVAssetExportSessionStatusFailed: %@", exportSession.error);
                break;
            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Export Cancelled");
                break;
        }
    }];
}
}
-(void)exportedvideoplay:(NSString*)url
{
    AVPlayer *avPlayer = [AVPlayer playerWithURL:[NSURL fileURLWithPath:url]];
    
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    layer.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view.layer addSublayer: layer];
    [avPlayer play];

}
-(void)playvideo:(NSString*)url{

    

    NSURL *fileURL = [NSURL fileURLWithPath:url];
    
    self.moviePlayerController=[[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    [self.moviePlayerController.view setFrame:CGRectMake(0, 0, 320, 560)];
    [self.moviePlayerController prepareToPlay];
    [self.moviePlayerController setShouldAutoplay:YES]; // And other options you can look through the documentation.
    self.moviePlayerController.controlStyle = MPMovieControlStyleFullscreen;
    self.moviePlayerController.scalingMode=MPMovieScalingModeFill;
    [self.moviePlayerController play];
    [self.view addSubview:self.moviePlayerController.view];

    [self.view bringSubviewToFront:self.tempDrawImage];
    [self.view bringSubviewToFront:self.mainImage];
    [self.view bringSubviewToFront:btnMix];
    [self.moviePlayerController setFullscreen:YES];
    
    
    /*self.moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(introMovieFinished:)
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:self.moviePlayerController];
     
     // Hide the video controls from the user
     [self.moviePlayerController setControlStyle:MPMovieControlStyleDefault];
     
     [self.moviePlayerController prepareToPlay];
     [self.moviePlayerController.view setFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
     [self.view addSubview:moviePlayerController.view];
     
     [self.moviePlayerController play];
     */

}
/*
- (void)introMovieFinished:(NSNotification *)notification
{
    NSLog(@"Video ended!");
}
*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:self.view];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    mouseSwiped = YES;
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.view];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [self.tempDrawImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    [self.tempDrawImage setAlpha:opacity];
    UIGraphicsEndImageContext();
    
    lastPoint = currentPoint;
    
   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(!mouseSwiped) {
        UIGraphicsBeginImageContext(self.view.frame.size);
        [self.tempDrawImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, opacity);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        self.tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIGraphicsBeginImageContext(self.mainImage.frame.size);
    [self.mainImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    [self.tempDrawImage.image drawInRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) blendMode:kCGBlendModeNormal alpha:opacity];
    self.mainImage.image = UIGraphicsGetImageFromCurrentImageContext();
    self.tempDrawImage.image = nil;
    UIGraphicsEndImageContext();
    
    
}

@end
