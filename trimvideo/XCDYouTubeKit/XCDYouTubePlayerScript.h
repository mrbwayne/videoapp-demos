//
//  Copyright (c) 2013-2014
//

#import <Foundation/Foundation.h>

__attribute__((visibility("hidden")))
@interface XCDYouTubePlayerScript : NSObject

- (instancetype) initWithString:(NSString *)string;

- (NSString *) unscrambleSignature:(NSString *)scrambledSignature;

@end
