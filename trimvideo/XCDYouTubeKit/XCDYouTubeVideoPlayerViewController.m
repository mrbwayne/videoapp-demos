//
//  Copyright (c) 2013-2014 
//

#import "XCDYouTubeVideoPlayerViewController.h"
#import "AppDelegate.h"

#import "XCDYouTubeClient.h"

#import <objc/runtime.h>

NSString *const XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey = @"error"; // documented in -[MPMoviePlayerController initWithContentURL:]

NSString *const XCDYouTubeVideoPlayerViewControllerDidReceiveMetadataNotification = @"XCDYouTubeVideoPlayerViewControllerDidReceiveMetadataNotification";
NSString *const XCDMetadataKeyTitle = @"Title";
NSString *const XCDMetadataKeySmallThumbnailURL = @"SmallThumbnailURL";
NSString *const XCDMetadataKeyMediumThumbnailURL = @"MediumThumbnailURL";
NSString *const XCDMetadataKeyLargeThumbnailURL = @"LargeThumbnailURL";
NSString *const XCDMetadataKeySelectedQualityURL = @"selectQualityVideos";


NSString *const XCDYouTubeVideoPlayerViewControllerDidReceiveVideoNotification = @"XCDYouTubeVideoPlayerViewControllerDidReceiveVideoNotification";


NSString *const XCDYouTubeVideoPlayerViewControllerDidReceiveVideoURLForCache = @"XCDYouTubeVideoPlayerViewControllerDidReceiveVideoURLForCache";


NSString *const XCDYouTubeVideoUserInfoKey = @"Video";

@interface XCDYouTubeVideoPlayerViewController ()
@property (nonatomic, weak) id<XCDYouTubeOperation> videoOperation;
@property (nonatomic, assign, getter = isEmbedded) BOOL embedded;
@end

@implementation XCDYouTubeVideoPlayerViewController
@synthesize delegate;
@synthesize isOnlyNeedVideoDownloadLink;

- (instancetype) init
{
	return [self initWithVideoIdentifier:nil];
}

- (instancetype) initWithContentURL:(NSURL *)contentURL
{
	@throw [NSException exceptionWithName:NSGenericException reason:@"Use the `initWithVideoIdentifier:` method instead." userInfo:nil];
}

- (instancetype) initWithVideoIdentifier:(NSString *)videoIdentifier
{
	if (!(self = [super init]))
		return nil;
	
	if (videoIdentifier)
		self.videoIdentifier = videoIdentifier;
	
	return self;
}

#pragma mark - Public

- (NSArray *) preferredVideoQualities
{
	if (!_preferredVideoQualities)
		_preferredVideoQualities = @[ XCDYouTubeVideoQualityHTTPLiveStreaming, @(XCDYouTubeVideoQualityHD720), @(XCDYouTubeVideoQualityMedium360), @(XCDYouTubeVideoQualitySmall240) ];
	
	return _preferredVideoQualities;
}

- (void) setVideoIdentifier:(NSString *)videoIdentifier
{
	if ([videoIdentifier isEqual:self.videoIdentifier])
		return;
	
	_videoIdentifier = [videoIdentifier copy];
	
	[self.videoOperation cancel];
	self.videoOperation = [[XCDYouTubeClient defaultClient] getVideoWithIdentifier:videoIdentifier completionHandler:^(XCDYouTubeVideo *video, NSError *error)
	{
        
//        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            // Add code here to do background processing
//            
//            dispatch_async( dispatch_get_main_queue(),
//            ^{
//                
                if (video)
                {
                    
                 
                    NSURL *streamURL = nil;
                    for (NSNumber *videoQuality in self.preferredVideoQualities)
                    {
                     
                        streamURL = video.streamURLs[videoQuality];
                        if (streamURL)
                        {
                            [self startVideo:video streamURL:streamURL];
                            break;
                        }
                    }
                    
                    if (!streamURL)
                    {
                        NSError *noStreamError = [NSError errorWithDomain:XCDYouTubeVideoErrorDomain code:XCDYouTubeErrorNoStreamAvailable userInfo:nil];
                        [self stopWithError:noStreamError];
                    }
                }
                else
                {
                    [self stopWithError:error];
                }
            
//            });
//        });

	}];
}

#pragma mark - Private

- (void) startVideo:(XCDYouTubeVideo *)video streamURL:(NSURL *)streamURL
{
    NSLog(@"streamURL @@@@@@@@@@@@@@@ %@",streamURL);
    
    video.selectQualityVideos=streamURL;

    if (self.isOnlyNeedVideoDownloadLink)
    {
        if ([delegate respondsToSelector:@selector(getDownloadLink:)])
        {
            [delegate getDownloadLink:streamURL];
            
            //[[NSNotificationCenter defaultCenter] postNotificationName:XCDYouTubeVideoPlayerViewControllerDidReceiveVideoURLForCache object:self userInfo:@{ XCDYouTubeVideoUserInfoKey: video }];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"XCDYouTubeVideoPlayerViewControllerDidReceiveVideoNotification" object:nil userInfo:@{ XCDYouTubeVideoUserInfoKey: video }];
    }
}
- (void) stopWithError:(NSError *)error
{
    NSLog(@"Error Error Error Error");
    NSLog(@"error :: %@",error);
    
    if (self.isOnlyNeedVideoDownloadLink)
    {
        // [appDelegate hideMBHUD];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:XCDYouTubeVideoPlayerViewControllerDidReceiveVideoNotification object:self userInfo:nil];
    }
}

#pragma mark - UIViewController
- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if (![self isBeingPresented])
		return;

}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	if (![self isBeingDismissed])
		return;
	
	[self.videoOperation cancel];
}
@end