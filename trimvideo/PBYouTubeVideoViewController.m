//
//  PBYouTubeVideoViewController.m
//
//  Created by Philippe Bernery on 08/02/13.
//  Copyright (c) 2013 Philippe Bernery. All rights reserved.
//

#import "PBYouTubeVideoViewController.h"


NSString *const PBYouTubePlayerEventReady = @"ready";
NSString *const PBYouTubePlayerEventStateChanged = @"stateChange";
NSString *const PBYouTubePlayerEventPlaybackQualityChanged = @"playbackQualityChange";
NSString *const PBYouTubePlayerEventPlaybackRateChanged = @"playbackRateChange";
NSString *const PBYouTubePlayerEventError = @"error";
NSString *const PBYouTubePlayerEventApiChange = @"apiChange";

#define LOG_ERROR(x) NSLog(@"[%@ %@] - error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), x)

const CGFloat YouTubeStandardPlayerWidth = 640;
const CGFloat YouTubeStandardPlayerHeight = 390;


@interface PBYouTubeVideoViewController () <UIWebViewDelegate>

@property (nonatomic, strong) NSString *videoId;
@property (nonatomic, strong) UIWebView *webView;

@end


@implementation PBYouTubeVideoViewController
@synthesize appDelegate;

- (id)initWithVideoId:(NSString *)videoId
{
    NSParameterAssert(videoId != nil);

    if ((self = [super initWithNibName:nil bundle:nil])) {
        self.videoId = videoId;
    }
    return self;
}

- (void)dealloc
{
    self.webView.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [self setViews];
    
    self.view.backgroundColor = [UIColor blackColor];

    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.backgroundColor = self.view.backgroundColor;
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}

-(void)setViews{
    
    //Hide Navigattion back
    self.navigationItem.hidesBackButton = YES;
    
    //setNavigation Title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textAlignment = NSTextAlignmentCenter;
    // ^-Use UITextAlignmentCenter for older SDKs.
    label.textColor = [UIColor whiteColor]; // change this color
    self.navigationItem.titleView = label;
    label.text = NSLocalizedString(@"Player", @"");
    [label sizeToFit];
    
    //set Navigationbar Color
    /*if (isIOS7) {
        self.navigationController.navigationBar.barTintColor = [appDelegate colorWithHexString:@"7DAB14"];
        self.navigationController.navigationBar.translucent = NO;
    }else {
        self.navigationController.navigationBar.tintColor = [appDelegate colorWithHexString:@"7DAB14"];
    }*/
     self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *btnMenu = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(btnMenu_Click:)];
    self.navigationItem.leftBarButtonItem = btnMenu;
}

-(IBAction)btnMenu_Click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.webView loadHTMLString:[self htmlContent] baseURL:nil];
    
    /*if(!appDelegate.isPaid)
        [appDelegate ShowFlurryAds:self];*/
}


    
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
   /* if(!appDelegate.isPaid)
        [appDelegate RemoveFlurryAd];*/
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    [self updatePlayerSize];
}

#pragma mark - Actions

- (void)play
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"player.playVideo();"];
}

- (void)pause
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"player.pauseVideo();"];
}

- (void)stop
{
    [self.webView stringByEvaluatingJavaScriptFromString:@"player.stopVideo();"];
}

#pragma mark - Accessors

- (void)setPlayerSize:(CGSize)playerSize
{
    [self.webView stringByEvaluatingJavaScriptFromString:
            [NSString stringWithFormat:@"player.setSize(%u, %u);",
                            (unsigned int) playerSize.width, (unsigned int) playerSize.height]];
}

- (NSTimeInterval)duration
{
    return [[self.webView stringByEvaluatingJavaScriptFromString:@"player.getDuration();"] doubleValue];
}

- (NSTimeInterval)currentTime
{
    return [[self.webView stringByEvaluatingJavaScriptFromString:@"player.getCurrentTime();"] doubleValue];
}

#pragma mark - Helpers

- (NSString *)htmlContent
{
    NSString *pathToHTML = [[NSBundle mainBundle] pathForResource:@"PBYouTubeVideoView" ofType:@"html"];
    NSAssert(pathToHTML != nil, @"could not find PBYouTubeVideoView.html");

    NSError *error = nil;
    NSString *template = [NSString stringWithContentsOfFile:pathToHTML encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        LOG_ERROR(error);
    }

    CGSize playerSize = [self playerSize];
    NSString *result = [NSString stringWithFormat:template,
                    [NSString stringWithFormat:@"%.0f", playerSize.width],
                    [NSString stringWithFormat:@"%.0f", playerSize.height],
                    self.videoId];
    return result;
}

- (void)updatePlayerSize
{
    CGSize playerSize = [self playerSize];
    [self setPlayerSize:playerSize];
}

- (CGSize)playerSize
{
    float heightRatio = self.view.bounds.size.height / YouTubeStandardPlayerHeight;
    float widthRatio = self.view.bounds.size.width / YouTubeStandardPlayerWidth;
    float ratio = MIN(widthRatio, heightRatio);

    CGSize playerSize = CGSizeMake(YouTubeStandardPlayerWidth * ratio, YouTubeStandardPlayerHeight * ratio);
    return playerSize;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request url>>>%@",request.URL);
    if ([[request URL].scheme isEqualToString:@"ytplayer"]) {
        NSArray *components = [[request URL] pathComponents];
        if ([components count] > 1) {
            NSString *actionName = components[1];
            NSString *actionData = nil;
            if ([components count] > 2) {
                actionData = components[2];
            }

            [self.delegate youTubeVideoViewController:self didReceiveEventNamed:actionName eventData:actionData];
        }
        return NO;
    } else {
        return YES;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    LOG_ERROR(error);
}

@end
