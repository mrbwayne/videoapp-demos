//
//  scrolldrag.h
//  trimvideo
//
//  Created by Anuj on 6/9/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "UIImage+Scale.h"
#import "UIImage+Resize.h"
#import "MyImageView.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MediaToolbox/MediaToolbox.h>
#import <QuartzCore/QuartzCore.h>
#import "TrimVideoClass.h"

@interface scrolldrag : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *tableview_filmstrip;
    __weak IBOutlet UITableView *tableview_gallary;
    IBOutlet MyImageView *imageBeingDragged;
    NSMutableArray *arrscrollitems;
    NSMutableArray *allVideos;
    NSMutableArray *allFilmStrips;
    NSMutableDictionary *dic;
    int k;
    int kNumberOfCellsInRowForFilStripTable;
    MyImageView *tempImageView;
    int counter;
    __weak IBOutlet UIView *viewTrash;
    NSMutableArray *arrayVideoPaths;
    
}
- (IBAction)buttonPlay_Click:(id)sender;


@end
