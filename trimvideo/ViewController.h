//
//  ViewController.h
//  trimvideo
//
//  Created by Anuj on 6/2/14.
//  Copyright (c) 2014 wayne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TrimVideoClass.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MediaToolbox/MediaToolbox.h>
#import <QuartzCore/QuartzCore.h>

@interface ViewController : UIViewController
{

    NSMutableArray *videoClipPaths;
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    BOOL mouseSwiped;
    
    __weak IBOutlet UIButton *btnMix;
    
}
- (IBAction)btnMix_Click:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UIImageView *tempDrawImage;
@property(nonatomic,retain) MPMoviePlayerController *moviePlayerController;
@end
